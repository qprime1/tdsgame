// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameGameMode.generated.h"

UCLASS(minimalapi)
class ATDSGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDSGameGameMode();
};



